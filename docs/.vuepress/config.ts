const { backToTopPlugin } = require('@vuepress/plugin-back-to-top');
const { searchPlugin } = require('@vuepress/plugin-search')
import { defineUserConfig,defaultTheme  } from 'vuepress';
export default defineUserConfig({
    lang: 'zh-CN',
    title: 'vuepress模板',
    description: 'vuepress模板',
    port: 8084,
    base: '/vuepress/',
    theme: defaultTheme({
      docsDir: 'docs',
      locales: {
        
      }
    }),
    plugins: [
        backToTopPlugin(),
        searchPlugin({
            locales: {
              '/': {
                placeholder: '搜索',
              }
            },
          }),
    ],
});